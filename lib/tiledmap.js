import Layer from "./layer";
import Tileset from "./tileset";
import { parseAttributes, parseXml } from "./xml";

class Tiledmap {
  constructor({ url, xmlElement, resourceLoader }) {
    const attributes = parseAttributes(xmlElement, {
      numericalAttributeNames: ["tileheight", "tilewidth", "height", "width"]
    });
    Object.assign(this, attributes);
    this.layers = [];
    this.tilesets = [];

    const loadLayer = layerElement => {
      const dataElement = layerElement.querySelector("data");
      const layerAttributes = {
        ...parseAttributes(layerElement, {
          numericalAttributeNames: ["height", "width"]
        }),
        map: this
      };

      const layer = Layer.parse(dataElement, layerAttributes);
      this.layers.push(layer);
    };

    const loadTileSet = tilesetElement => {
      const tilesetAttributes = {
        ...parseAttributes(tilesetElement, {
          numericalAttributeNames: ["firstgid"]
        })
      };

      this.tilesets.push({
        ...tilesetAttributes,
        isLoading: true
      });
      const tilesetIndex = this.tilesets.length - 1;

      if (tilesetAttributes.source) {
        tilesetAttributes.source = `${url}/../${tilesetAttributes.source}`;
        resourceLoader
          .load(tilesetAttributes.source)
          .then(({ url, data: xmlString }) => {
            const tileset = Tileset.parse(url, xmlString, tilesetAttributes);
            this.tilesets[tilesetIndex] = tileset;
            this.updateIsLoading();
          });
      } else {
        throw new Error("Only external tilesets are supported!");
      }
    };

    const layerElements = xmlElement.querySelectorAll("layer");
    layerElements.forEach(loadLayer);

    const tilesetElements = xmlElement.querySelectorAll("tileset");
    tilesetElements.forEach(loadTileSet);

    this.isLoading = tilesetElements.length > 0;
  }

  updateIsLoading() {
    this.isLoading = this.tilesets.some(tileset => tileset.isLoading);
  }

  static parse(url, xmlString, resourceLoader) {
    if (!url) {
      throw new Error("Missing URL!");
    }
    if (!xmlString) {
      throw new Error("Missing XML string!");
    }

    const tiledmapElement = parseXml(xmlString);
    const tiledmap = new Tiledmap({
      url,
      xmlElement: tiledmapElement,
      resourceLoader
    });
    return tiledmap;
  }
}

export default Tiledmap;
