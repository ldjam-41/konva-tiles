import { parseAttributes, parseXml } from "./xml";

class Tileset {
  constructor({ url, xmlElement, attributes }) {
    Object.assign(this, attributes);
    Object.assign(
      this,
      parseAttributes(xmlElement, {
        numericalAttributeNames: [
          "columns",
          "margin",
          "spacing",
          "tilecount",
          "tileheight",
          "tilewidth"
        ]
      })
    );

    const imageElement = xmlElement.querySelector("image");
    this.image = parseAttributes(imageElement, {
      numericalAttributeNames: ["height", "width"]
    });

    const { source: imageSource } = this.image;
    if (imageSource) {
      this.image.source = `${url}/../${imageSource}`;
    }
  }

  static parse(url, xmlString, attributes = {}) {
    if (!url) {
      throw new Error("Missing URL!");
    }
    if (!xmlString) {
      throw new Error("Missing XML string!");
    }

    const tilesetElement = parseXml(xmlString);
    const tileset = new Tileset({
      url,
      xmlElement: tilesetElement,
      attributes
    });
    return tileset;
  }

  checkTileIdRange(tileId) {
    if (!this.isInTileIdRange(tileId)) {
      throw new Error(
        `Tile ID ${tileId} out of range! (${this.firstgid}, ${this.firstgid +
          this.tilecount})`
      );
    }
  }

  isInTileIdRange(tileId) {
    return this.firstgid <= tileId && tileId < this.firstgid + this.tilecount;
  }

  tileX(tileId) {
    this.checkTileIdRange(tileId);
    const tileColumn = (tileId - this.firstgid) % this.columns;
    return this.margin + tileColumn * (this.spacing + this.tilewidth);
  }

  tileY(tileId) {
    this.checkTileIdRange(tileId);
    const tileRow = Math.floor((tileId - this.firstgid) / this.columns);
    return this.margin + tileRow * (this.spacing + this.tileheight);
  }
}

export default Tileset;
