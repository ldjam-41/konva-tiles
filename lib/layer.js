import pako from "pako";
import { parseAttributes } from "./xml";

class Layer {
  constructor({ tileIds, attributes }) {
    Object.assign(this, attributes);
    Object.assign(this, { tileIds });
  }

  static parse(dataElement, attributes = {}) {
    const dataAttributes = parseAttributes(dataElement);

    if (dataAttributes.encoding !== "base64") {
      throw new Error("Only Base64 encoding is supported!");
    }

    if (dataAttributes.compression !== "zlib") {
      throw new Error("Only zlib compression is supported!");
    }

    const binaryData = atob(dataElement.textContent.trim());
    const decompressedData = pako.inflate(binaryData);

    const tileIds = [];
    for (let i = 0; i < decompressedData.length; i += 4) {
      let tileIndex;
      tileIndex = decompressedData[i + 3];
      tileIndex = (tileIndex << 8) | decompressedData[i + 2];
      tileIndex = (tileIndex << 8) | decompressedData[i + 1];
      tileIndex = (tileIndex << 8) | decompressedData[i + 0];
      tileIds.push(tileIndex);
    }

    const layer = new Layer({ tileIds, attributes });
    return layer;
  }
}

export default Layer;
