class ResourceLoader {
  load(url) {
    if (!process.browser) {
      throw new Error("Currently only browser is supported!");
    }

    return fetch(url)
      .then(res => res.text())
      .then(data => ({ url, data }));
  }
}

export default ResourceLoader;
