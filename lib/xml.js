export const parseAttributes = (
  element,
  { numericalAttributeNames } = { numericalAttributeNames: [] }
) => {
  const attributes = {};

  for (let attribute of element.attributes) {
    if (numericalAttributeNames.includes(attribute.name)) {
      attributes[attribute.name] = parseInt(attribute.value, 10);
    } else {
      attributes[attribute.name] = attribute.value;
    }
  }

  return attributes;
};

export const parseXml = xmlString => {
  if (!process.browser) {
    throw new Error("Currently only browser is supported!");
  }

  const parser = new DOMParser();
  const xmlDocument = parser.parseFromString(xmlString, "application/xml");
  return xmlDocument.documentElement;
};
